package com.company.codecs;

import com.company.entity.Student;

public class StringToStudent implements Codec<Student> {
    public Student decode(String line) {
        String[] strings = line.split(" ");
        return new Student((Integer.parseInt(strings[0])), strings[1], strings[2], null /* checkGroup(Integer.parseInt(strings[3])) */);
    }

    public String encode(Student line) {
        return null;
    }
}
