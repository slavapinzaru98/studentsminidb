package com.company.codecs;

public interface Codec<T> {
    public T decode(String line);
    public String encode(T entity);
}
