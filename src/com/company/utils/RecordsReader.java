package com.company.utils;

import com.company.codecs.Codec;
import com.company.entity.Group;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class RecordsReader <T> {


    public List<T> readFromFile(File file, Codec<T> codec) throws IOException {
        List<T> entitiesList = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line;
        String[] strings;
        while ((line = bufferedReader.readLine()) != null) {
            entitiesList.add(codec.decode(line));
        }

        bufferedReader.close();
        return entitiesList;
    }


    public List<T> readFromFile(File file, Function<String, T> transform) throws IOException {
        List<T> entitiesList = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line;
        String[] strings;
        while ((line = bufferedReader.readLine()) != null) {
            entitiesList.add(transform.apply(line));
        }

        bufferedReader.close();
        return entitiesList;
    }
}
