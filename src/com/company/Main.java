package com.company;

import com.company.entity.Group;
import com.company.entity.Student;
import com.company.entity.Type;

import java.io.*;
import java.io.IOException;
import java.util.*;

public class Main {
    public static List<Student> studentList = new LinkedList<>();
    public static List<Group> groupList = new LinkedList<>();
    public static File fileStudent = new File("students0");
    public static File fileGroup = new File("group0");

    public static void main(String[] args) throws Exception {
        initializationFromGroupFile();
        initializationFromStudentFile();

        int variant;
        do {
            System.out.printf("\n\tMenu\n");
            System.out.printf("choose variant:\n" +
                    "Menu for Student = 1 \n" +
                    "Menu for Group = 2 \n"
            );

            Scanner scanner = new Scanner(System.in);
            variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    showStudentOptions();
                    break;
                case 2:
                    showGroupOptions();
                    break;
                default:
                    break;
            }
        }
        while (variant != 0);
    }

    private static void showStudentOptions() throws IOException {
        int variant;
        do {
            System.out.printf("\n\tMenu for Student \n");
            System.out.printf("choose variant:\n" +
                    "pass to group \n" +
                    " \t1) get \n" +
                    " \t2) delete\n" +
                    " \t3) update\n" +
                    " \t4) create\n"
            );

            Scanner scanner = new Scanner(System.in);
            variant = scanner.nextInt();

            switch (variant) {
                case 1:
                    showStudent();
                    break;
                case 2:
                    deleteStudent();
                    break;
                case 3:
                    updateStudent();
                    break;
                case 4:
                    createStudent();
                    break;
                default:
                    break;
            }
        } while (variant != 0);
    }

    private static void showGroupOptions() throws IOException {
        int variant;
        do {
            System.out.printf("\n\tMenu for group \n");
            System.out.printf("choose variant:\n" +
                    "pass to group \n" +
                    " \t1) get \n" +
                    " \t2) delete\n" +
                    " \t3) update\n" +
                    " \t4) create\n"
            );

            Scanner scanner = new Scanner(System.in);
            variant = scanner.nextInt();

            switch (variant) {
                case 1:
                    showGroup();
                    break;
                case 2:
                    deleteGroup();
                    break;
                case 3:
                    updateGroup();
                    break;
                case 4:
                    createGroup();
                    break;
                default:
                    break;
            }
        } while (variant != 0);
    }

    public static Group checkGroup(Integer id) {
        for (Group group : groupList) {
            if (group.getId() == id.intValue()) {
                return group;
            }
        }
        return null;
    }

    public static void initializationFromStudentFile() throws IOException {
        FileReader fileReader = new FileReader(fileStudent);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        String[] strings;
        while ((line = bufferedReader.readLine()) != null) {
//            System.out.println(line);
            strings = line.split(" ");
            Student student = new Student((Integer.parseInt(strings[0])), strings[1], strings[2], checkGroup(Integer.parseInt(strings[3])));
            studentList.add(student);
        }
        System.out.println("Инициализация данных из файла : " + fileStudent.getName());
        Iterator<Student> iterator = studentList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static Type whichType(String s) {
        Type typeFromFile = Type.valueOf(s);
        if (Type.FR == typeFromFile)
            return Type.FR;
        else return Type.FZ;
    }

    private static void initializationFromGroupFile() throws IOException {
        FileReader fileReader = new FileReader(fileGroup);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        String[] strings;
        while ((line = bufferedReader.readLine()) != null) {
            strings = line.split(" ");
            Group group = new Group((Integer.parseInt(strings[0])), strings[1], whichType(strings[2]));
            groupList.add(group);
        }
        System.out.println("Инициализация данных из файла : " + fileGroup.getName());
        Iterator<Group> iterator = groupList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void deleteStudent() throws IOException {
        System.out.println("Введите id которое хотите удалить: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        Iterator<Student> iterator = studentList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId() == id) {
                iterator.remove();
                System.out.println("Удаление произошло успешно");
                saveAllStudentChanges();
            }
        }

//        saveAllChanges(file, list.stream().filter(element ->
//                element.getId() != new Scanner(System.in).nextInt()).collect(Collectors.toList()));
    }

    private static void deleteGroup() throws IOException {
        System.out.println("Введите id которое хотите удалить: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        Iterator<Group> iterator = groupList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId() == id) {
                iterator.remove();
                System.out.println("Удаление произошло успешно");
                saveAllGroupChanges();
            }
        }
    }

    public static void updateStudent() throws IOException {
        System.out.println("Введите id которое хотите изменить: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        for (Student student : studentList) {
            if ((student.getId()) != id) {
                continue;
            }
            System.out.println("Осуществляется изменение над : " + student.toString());
            student.setFirstName(scanner.next());
            student.setLastName(scanner.next());
            student.setGroup(checkGroup(scanner.nextInt()));
            System.out.println("Изменения произведены : " + student.toString());
            saveAllStudentChanges();
        }
    }

    public static void updateGroup() throws IOException {
        System.out.println("Введите id которое хотите изменить: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        for (Group group : groupList) {
            if ((group.getId()) != id) {
                continue;
            }
            System.out.println("Осуществляется изменение над : " + group.toString());
            group.setName(scanner.next());
            group.setType(whichType(scanner.next()));
            System.out.println("Изменения произведены : " + group.toString());
            saveAllGroupChanges();
        }
    }

    public static void createStudent() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводите данные созданному экземпляру: ");
        Student student = new Student(scanner.nextInt(), scanner.next(), scanner.next(), checkGroup(scanner.nextInt()));
        studentList.add(student);
        System.out.println("Данные сохранены");
        saveAllStudentChanges();
    }

    public static void createGroup() throws IOException {
        Scanner scanner = new Scanner((System.in));
        System.out.println("Вводите данные созданному экземпляру: ");
        Group group = new Group(scanner.nextInt(), scanner.next(), whichType(scanner.next()));
        groupList.add(group);
        System.out.println("Данные сохранены");
        saveAllGroupChanges();
    }

    public static void saveAllStudentChanges() throws IOException {
        FileWriter fileWriter = new FileWriter(fileStudent);
        int id;
        String firstName;
        String lastName;
        int group;
        for (Student student : studentList) {
            id = student.getId();
            firstName = student.getFirstName();
            lastName = student.getLastName();
            group = student.getGroup().getId();
            fileWriter.write(id + " " + firstName + " " + lastName + " " + group + "\n");
        }
        fileWriter.close();
    }

    private static void saveAllGroupChanges() throws IOException {
        FileWriter fileWriter = new FileWriter(fileGroup);
        int id;
        String name;
        String type;
        for (Group group : groupList) {
            id = group.getId();
            name = group.getName();
            type = group.getType().getTitle();
            fileWriter.write(id + " " + name + " " + type + "\n");
        }
        fileWriter.close();
    }

    public static void showStudent() {
        System.out.println("Вывод всех имеющихся данных:");
        studentList.forEach(System.out::println);
    }

    public static void showGroup() {
        System.out.println("Вывод всех имеющихся данных:");
        for (Group group : groupList) {
            System.out.println(group);
        }
    }
//    public static void showList(String entity){
//        List listToShow = new ArrayList();
//        if(STUDENT.equalsIgnoreCase(entity)){
//          listToShow = studentList;
//        }else if (GROUP.equalsIgnoreCase(entity)){
//            listToShow = groupList;
//        }
//        listToShow.forEach(System.out::println);
//    }

}
