package com.company.entity;

public enum Type {
    FZ ("FZ"),
    FR ("FR");
    private String title;
    Type(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Type{" +
                "title='" + title + '\'' +
                '}';
    }
}
